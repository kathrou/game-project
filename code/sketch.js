/*

The Game Project 7

Extensions: 
- Sound
- Platforms
- Enemies 
- 2 different levels (level 2 adds: More enemies, different colors, more collectables, oxygen level decreases faster)
- Used p5.play.js to create my enemies

What I found difficult? 
- I struggled a bit implementing the p5.play.js library. I used the collision feature the library 
offers to check whether my sprite and character were colliding and it was a challenge to fully understand it.

Skills I learnt:
- A new library
- Got a better understanding of constructor functions 
- Understood better how to use methods within objects to simplify my code

What I found interesting:
- All the bugs there were to fix! I learnt a lot from fixing the bugs and improving my code

*/

// Declaring variables
let floorPos_y;
let scrollPos;
let gameChar_world_x;
let isLeft;
let isRight;
let isFalling;
let isPlummeting;
let mountains;
let trees;
let canyons;
let collectable;
let color1;
let color2;
let stars;
let planets;
let rocket;
let lives;
let level;
let stones;
let character;
let platforms;
let alien1;
let alien2;
let alien3;
let alien4;
let dieSoundPlaying;
let overSoundPlaying;
let winSoundPlaying;
let completeSoundPlaying;
let air;
let maxAir;
let start;
let oxygenDisplay;
let gameCompleted;

// Preload sounds
function preload() {
  soundFormats("mp3", "wav");

  //load sounds
  //Jump sounds
  jumpSound = loadSound("jump.mp3");
  jumpSound.setVolume(0.1);
  //Die Sounds
  dieSound = loadSound("die.mp3");
  dieSound.setVolume(0.1);

  //Game Over sound
  overSound = loadSound("over.wav");
  overSound.setVolume(0.1);

  //Collecting oxygen sound
  collectableSound = loadSound("collectable.wav");
  collectableSound.setVolume(0.1);

  //Level completed
  winSound = loadSound("win.wav");
  winSound.setVolume(0.1);

  // Game Completed
  completeSound = loadSound("game_complete.wav");
  completeSound.setVolume(0.1);
}

function setup() {
  createCanvas(1024, 576);
  floorPos_y = (height * 3) / 4;
  lives = 3; // Start with 3 lives
  level = 1; // Start at Level 1
  air = 300; // Start oxygen at 300
  maxAir = 350; 
  oxygenDisplay = true;
  gameCompleted = false;
  startGame();
}

function draw() {
  //background color gradient
  let newColor = lerpColor(color1, color2, 0.8);
  background(newColor); // fill the sky gradient with new color

  // call function to draw stars
  stars.drawStars();

  // draw ground
  noStroke();
  fill(219);
  rect(0, floorPos_y, width, height / 4);

  // Check if character should die
  checkPlayerDie();

  //Check how much oxygen is left
  airCheck();

  //Saves the current drawing style settings and transformations
  push();

  // Displace objects by an amount equal to scrollPos in the horizontal direction and 0 in the vertical direction
  translate(scrollPos, 0);

  // draw planets
  planets.draw();

  // Draw mountains
  if (level == 1) {
    mountains.draw(64, 64, 64);
  }
  if (level == 2) {
    mountains.draw(125, 79, 53); //change mountains color for level 2
  }

  // Draw trees
  if (level == 1) {
    trees.draw(45, 42, 74);
  }
  if (level == 2) {
    trees.draw(219, 50, 43);//change trees color for level 2
  }

  // Add sign that guides the player to rocket
  textSize(12);
  fill(219, 219, 219);
  rect(570, 345, 10, 90);
  fill(255);
  rect(580, 345, 90, 20);
  triangle(670, 345, 670, 365, 690, 355);
  fill(0);
  text("Rocket this way", 585, 358);

  // Add sign that tells player they are going the wrong way
  textSize(12);
  fill(219, 219, 219);
  rect(-600, 345, 10, 90);
  fill(255);
  let w = textWidth("Wrong way, go back!")
  rect(-590, 345, w + 5, 20);
  fill(0);
  text("Wrong way, go back!", -590, 358);

  // Draw canyons
  for (let i = 0; i < canyons.array.length; i++) {
    canyons.draw(canyons.array[i]);
    canyons.check(canyons.array[i]);
  }

  // Draw platforms
  for (let i = 0; i < platforms.array.length; i++) {
    platforms.array[i].draw();
  }

  // Make character fall fast if plummeting
  if (isPlummeting == true) {
    character.y_pos += 5;
    character.sprite.position.y += 5;
  }

  // Draw collectable items
  collectable.draw();

  // Check if player is close to the rocket
  if (!rocket.isReached) {
    rocket.checkRocket();
  }

  // Draw Rocket
  rocket.renderRocket();

  // Draw Stones
  stones.draw();

  // Draw aliens according to level
  if (level == 1) {
    alien1.draw();
    alien2.draw();
  }
  if (level == 2) {
    alien1.draw();
    alien2.draw();
    alien3.draw();
    alien4.draw();
  }

  // Draw a transparent sprite behind the character to check for collision using p5.play
  drawSprite(character.sprite);

  // Insert pop to reset the drawing to its previous settings.
  pop();

  // Add level keeper
  fill(255);
  textSize(24);
  text("Level " + level, width / 2 - 20, 30);

  // Draw lives counter
  livesCounter.draw();

  // Draw game character.
  character.draw();

  //LOGIC FOR THE GAME TO WORK

  // Make sure game re-starts when lives are below 1
  if (lives < 1) {
    textSize(32);
    text("Game over. Press space to re-start.", 290, height / 2);
    if (overSoundPlaying == false) {
      overSound.play();
      overSoundPlaying = true;
    }
    return;
  }

  // Show level completed text when player reaches rocket in level 1

  if (rocket.isReached && level == 1) {
    textSize(32);
    textStyle();
    text("Level Complete. Press space to continue.", 290, height / 2);
    if (winSoundPlaying == false) {
      winSound.play();
      winSoundPlaying = true;
    }
    return;
  }

  // End game when player finishes level 2
  if (rocket.isReached && level == 2) {
    textSize(32);
    let gameEnd1 = "Great work, you have completed the game!";
    let gameEnd2 = "Press space to re-start.";
    let wGameEnd1 = textWidth(gameEnd1);
    let wGameEnd2 = textWidth(gameEnd2);
    text(gameEnd1, 200, 150);
    text(gameEnd2, 200 + wGameEnd1 / 2 - wGameEnd2 / 2, 190);
    if (completeSoundPlaying == false) {
      completeSound.play();
      completeSoundPlaying = true;
    }
    return;
  }

  // Add oxygen bar
    oxygen.draw();

  // Logic to make the game character move or the background scroll.
  if (isLeft && !isPlummeting) {
    character.sprite.position.x -= 4;
    if (character.x_pos > width * 0.2) {
      character.x_pos -= 4;
    } else {
      scrollPos += 4;
    }
  }

  if (isRight && !isPlummeting) {
    character.sprite.position.x += 4;
    if (character.x_pos < width * 0.8) {
      character.x_pos += 4;
    } else {
      scrollPos -= 4; // negative for moving against the background
    }
  }

  // Logic to make the game character rise and fall. Ensure the character is able to jump from the platforms
  if (character.y_pos < floorPos_y) {
    let isContact = false;
    for (let i = 0; i < platforms.array.length; i++) {
      if (platforms.array[i].checkContact(gameChar_world_x, character.y_pos)) {
        isContact = true;
        isFalling = false;
        break;
      }
    }
    if (isContact == false) {
      character.y_pos += 1;
      character.sprite.position.y += 1;
      isFalling = true;
    }
  } else {
    isFalling = false;
  }

  // Update real position of gameChar for collision detection.
  gameChar_world_x = character.x_pos - scrollPos;
}

// ---------------------
// Key control functions
// ---------------------

function keyPressed() {
  // if statements to control the animation of the character when
  // keys are pressed.

  //right key is pressed
  if (keyCode == 39) {
    isRight = true;
  }

  //left key is pressed
  if (keyCode == 37) {
    isLeft = true;
  }

  // Make the game character jump when space key is pressed
  if (keyCode == 32) {
    if (!isFalling) {
      character.y_pos -= 100;
      character.sprite.position.y -= 100;
      jumpSound.play();
    }
  }

  //Check if level 1 has been completed and start level 2
  if (rocket.isReached && keyCode == 32 && level < 2) {
    nextLevel();
    startGame();
    
  }

  // Check if Game is completed and restart
  if ((rocket.isReached && keyCode == 32 && level == 2) || 
  (lives < 1 && keyCode == 32)) //Check if there are no lives left and re-start game
  {
    startGame();
    level = 1;
    lives = 3;
    air = 300;
  }
}

function keyReleased() {
  // if statements to control the animation of the character when
  // keys are released.

  //right key is released
  if (keyCode == 39) {
    isRight = false;
  }

  //left key is released
  if (keyCode == 37) {
    isLeft = false;
  }
}

// Function to ensure the character loses lives 
function checkPlayerDie() {
  if (character.y_pos >= height) {//Ensure character loses a life when it falls down a canyon
    lives -= 1;
    if (lives > 0) {
      startGame();
    }
  }
  if (air >= 0 && air <= 50) { //Ensure character loses a life when air is below 0
    if (frameCount % 200 === 0) { // Ensure character keeps losing a life every few seconds if they don't collect more oxygen
      lives -= 1;
      if (!dieSound.isPlaying() && lives > 0) {
        dieSound.play()
      }
    }
  }
}

// Function to reduce the air as time goes by. To make level 2 more difficult, we decrease the air by a greater amount
function airCheck() {
  if (!gameCompleted) {//Ensure air drops only if the game is still running. It should stop once game ends or level is completed
    if (level == 2) {
      air = constrain(air - 0.2, 0, 350); // Decrease air by 0.2 when level is 2. Ensure air doesn't go below 0 or above 350
    } else if (level == 1) {
      air = constrain(air - 0.1, 0, 350);// Decrease air by 0.1 when level is 1. Ensure air doesn't go below 0 or above 350
    }
  }
}

// Function to go to next level and start the oxygen level at 300 again 
function nextLevel() {
  level += 1;
  air = 300;
}

function startGame() {
  //Create a constructor function for my enemy "The Alien"
  function Alien(start_x, end_x) {
    //Start_x and end_x in this function is the range in which my sprite moves within the world
    // (ie. 500, 1000 means the alien moves only from 500 pixels until it reaches 1000 and then turns back)
    //Create the sprite
    this.sprite = createSprite(start_x, floorPos_y - 30);
    this.sprite.addImage(
      loadImage("ghost_standing0001.png", "ghost_standing0007.png")
    );
    this.sprite.addAnimation(
      "floating",
      "ghost_standing0001.png",
      "ghost_standing0007.png"
    );
    this.sprite.addAnimation(
      "moving",
      "ghost_walk0001.png",
      "ghost_walk0004.png"
    );
    this.sprite.changeAnimation("moving");
    this.walkForward = true;
    this.velocity = 2;
    this.sprite.velocity.x = this.velocity;

    this.spriteToDamage = null;
    this.onDamageCallback = null;

    this.sprite.scale = 0.4;
    
    this.changeDirection = function () {
      if (this.walkForward) {
        this.sprite.mirrorX(-1);
        this.sprite.velocity.x = -1 * this.velocity;
        this.walkForward = false;
      } else {
        this.sprite.mirrorX(1);
        this.sprite.velocity.x = this.velocity;
        this.walkForward = true;
      }
    };

    // When the alien reaches the end of its patrol, reverse its direction and vice versa
    this.draw = function () {
      if (
        this.walkForward &&
        this.sprite.position.x + this.sprite.width >= end_x
      ) {
        this.changeDirection();
      }
      if (!this.walkForward && this.sprite.position.x <= start_x) {
        this.changeDirection();
      }
      drawSprite(this.sprite);
      // Check for overlapping sprite
      this.sprite.overlap(this.spriteToDamage, this.onDamageCallback);
    };
  /*
      Method to register another sprite for which to check if there's an overlap.
      When an overlap occurs, call the onDamageCallback function that was passed
      into this function
    */
    this.onOverlap = function (sprite, onDamageCallback) {
      this.spriteToDamage = sprite;
      this.onDamageCallback = onDamageCallback;
    };
  }

  // Create Aliens using constructor function
  alien1 = new Alien(700, 1000);
  alien2 = new Alien(2000, 2650);
  alien3 = new Alien(1350, 1700);
  alien4 = new Alien(3800, 4400);

  //Add transparent sprite behind my character drawing to allow for easy collision detection
  let characterSprite = createSprite(width / 2, floorPos_y - 35, 50, 72);
  characterSprite.addImage(loadImage("white-background.png"));

  //Character object
  character = {
    sprite: characterSprite,
    x_pos: width / 2,
    y_pos: floorPos_y,
    draw: function () {
      // draw game character
      if (isLeft && isFalling) {

        // draw face

        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        ellipse(this.x_pos, this.y_pos - 63, 20, 20);

        strokeWeight(2);
        stroke(102, 84, 86);
        fill(255, 199, 169);
        arc(this.x_pos - 8, this.y_pos - 63, 10, 10, (PI * 3) / 2, PI / 2); // 180 degrees

        // draw eyes
        noStroke();
        fill(102, 84, 86);
        ellipse(this.x_pos - 6, this.y_pos - 63, 2, 3);

        // draw oxygen
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(177, 177, 177);
        rect(this.x_pos + 8, this.y_pos - 51, 8, 20, 3);
        rect(this.x_pos + 8, this.y_pos - 31, 8, 5, 3);

        // draw body
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        rect(this.x_pos - 10, this.y_pos - 53, 17, 22, 5);
        rect(this.x_pos - 10, this.y_pos - 31, 17, 8, 5);

        // draw legs
        //left
        rect(this.x_pos - 13.5, this.y_pos - 25, 10, 8, 3);
        rect(this.x_pos - 15, this.y_pos - 17, 10, 8, 3);
        rect(this.x_pos - 16.5, this.y_pos - 9, 10, 6, 3);

        //right
        rect(this.x_pos - 3, this.y_pos - 23, 10, 8, 3);
        rect(this.x_pos - 1, this.y_pos - 15, 10, 8, 3);
        rect(this.x_pos, this.y_pos - 7, 10, 6, 3);

        // draw arms
        rect(this.x_pos + 2, this.y_pos - 51, 8, 10, 3);
        rect(this.x_pos + 4, this.y_pos - 41, 8, 10, 3);
        rect(this.x_pos + 6, this.y_pos - 31, 8, 5, 3);

        // draw logo

        noStroke();
        fill(255, 0, 255);
        ellipse(this.x_pos - 6, this.y_pos - 48, 5, 5);

        fill(244, 243, 245);
        ellipse(this.x_pos - 6, this.y_pos - 48, 2, 2);

        //draw cable
        fill(70, 130, 180);
        stroke(0);
        strokeWeight(0.2);
        beginShape();
        curveVertex(this.x_pos + 12, this.y_pos - 51);
        curveVertex(this.x_pos + 13, this.y_pos - 51);
        curveVertex(this.x_pos + 13, this.y_pos - 58);
        curveVertex(this.x_pos + 10, this.y_pos - 65);
        curveVertex(this.x_pos + 10, this.y_pos - 63);
        curveVertex(this.x_pos + 12, this.y_pos - 58);
        endShape(CLOSE);
      } else if (isRight && isFalling) {
        
        // draw face

        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        ellipse(this.x_pos, this.y_pos - 63, 20, 20);

        strokeWeight(2);
        stroke(102, 84, 86);
        fill(255, 199, 169);
        // ellipse(this.x_pos  , this.y_pos - 60, 10, 10);
        arc(this.x_pos + 8, this.y_pos - 63, 10, 10, PI / 2, (3 * PI) / 2); // 180 degrees

        // draw eyes
        noStroke();
        fill(102, 84, 86);
        ellipse(this.x_pos + 6, this.y_pos - 63, 2, 3);

        // draw oxygen
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(177, 177, 177);
        rect(this.x_pos - 16, this.y_pos - 51, 8, 20, 3);
        rect(this.x_pos - 16, this.y_pos - 31, 8, 5, 3);

        // draw body
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        rect(this.x_pos - 8, this.y_pos - 53, 17, 22, 5);
        rect(this.x_pos - 8, this.y_pos - 31, 17, 8, 5);

        // draw legs
        // left
        rect(this.x_pos - 10.5, this.y_pos - 23, 10, 8, 3);
        rect(this.x_pos - 12, this.y_pos - 15, 10, 8, 3);
        rect(this.x_pos - 13.5, this.y_pos - 7, 10, 6, 3);

        // right
        rect(this.x_pos, this.y_pos - 25, 10, 8, 3);
        rect(this.x_pos + 5, this.y_pos - 18, 10, 8, 3);
        rect(this.x_pos + 7, this.y_pos - 10, 10, 6, 3);

        // draw arms
        rect(this.x_pos - 9, this.y_pos - 51, 8, 10, 3);
        rect(this.x_pos - 11, this.y_pos - 41, 8, 10, 3);
        rect(this.x_pos - 13, this.y_pos - 31, 8, 5, 3);

        // draw logo

        noStroke();
        fill(255, 0, 255);
        ellipse(this.x_pos + 5, this.y_pos - 48, 5, 5);

        fill(244, 243, 245);
        ellipse(this.x_pos + 5, this.y_pos - 48, 2, 2);

        //draw cable
        fill(70, 130, 180);
        stroke(0);
        strokeWeight(0.2);
        beginShape();
        curveVertex(this.x_pos - 12, this.y_pos - 51);
        curveVertex(this.x_pos - 13, this.y_pos - 51);
        curveVertex(this.x_pos - 13, this.y_pos - 58);
        curveVertex(this.x_pos - 10, this.y_pos - 65);
        curveVertex(this.x_pos - 10, this.y_pos - 63);
        curveVertex(this.x_pos - 12, this.y_pos - 58);
        endShape(CLOSE);
      } else if (isLeft) {
        
        // draw face

        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        ellipse(this.x_pos, this.y_pos - 60, 20, 20);

        strokeWeight(2);
        stroke(102, 84, 86);
        fill(255, 199, 169);
        // ellipse(this.x_pos  , this.y_pos - 60, 10, 10);
        arc(this.x_pos - 8, this.y_pos - 60, 10, 10, (PI * 3) / 2, PI / 2); // 180 degrees

        // draw eyes
        noStroke();
        fill(102, 84, 86);
        ellipse(this.x_pos - 6, this.y_pos - 60, 2, 3);

        // draw body
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        rect(this.x_pos - 10, this.y_pos - 50, 17, 22, 5);
        rect(this.x_pos - 10, this.y_pos - 28, 17, 8, 5);

        // draw legs
        rect(this.x_pos - 10.5, this.y_pos - 20, 10, 8, 3);
        rect(this.x_pos - 3, this.y_pos - 20, 10, 8, 3);
        rect(this.x_pos - 12, this.y_pos - 12, 10, 8, 3);
        rect(this.x_pos - 12, this.y_pos - 12, 10, 8, 3);
        rect(this.x_pos - 1, this.y_pos - 12, 10, 8, 3);
        rect(this.x_pos - 13.5, this.y_pos - 4, 10, 6, 3);
        rect(this.x_pos, this.y_pos - 4, 10, 6, 3);

        // draw arms
        rect(this.x_pos, this.y_pos - 48, 8, 10, 3);
        rect(this.x_pos, this.y_pos - 38, 8, 10, 3);
        rect(this.x_pos, this.y_pos - 28, 8, 5, 3);

        // draw oxygen
        fill(177, 177, 177);
        rect(this.x_pos + 8, this.y_pos - 48, 8, 20, 3);
        rect(this.x_pos + 8, this.y_pos - 28, 8, 5, 3);

        // draw logo

        noStroke();
        fill(255, 0, 255);
        ellipse(this.x_pos - 6, this.y_pos - 45, 5, 5);

        fill(244, 243, 245);
        ellipse(this.x_pos - 6, this.y_pos - 45, 2, 2);

        //draw cable
        fill(70, 130, 180);
        stroke(0);
        strokeWeight(0.2);
        beginShape();
        curveVertex(this.x_pos + 12, this.y_pos - 48);
        curveVertex(this.x_pos + 13, this.y_pos - 48);
        curveVertex(this.x_pos + 13, this.y_pos - 55);
        curveVertex(this.x_pos + 10, this.y_pos - 62);
        curveVertex(this.x_pos + 10, this.y_pos - 60);
        curveVertex(this.x_pos + 12, this.y_pos - 55);
        endShape(CLOSE);
      } else if (isRight) {
        // draw face

        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        ellipse(this.x_pos, this.y_pos - 60, 20, 20);

        strokeWeight(2);
        stroke(102, 84, 86);
        fill(255, 199, 169);
        // ellipse(this.x_pos  , this.y_pos - 60, 10, 10);
        arc(this.x_pos + 8, this.y_pos - 60, 10, 10, PI / 2, (3 * PI) / 2); // 180 degrees

        // draw eyes
        noStroke();
        fill(102, 84, 86);
        ellipse(this.x_pos + 6, this.y_pos - 60, 2, 3);

        // draw oxygen
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(177, 177, 177);
        rect(this.x_pos - 16, this.y_pos - 48, 8, 20, 3);
        rect(this.x_pos - 16, this.y_pos - 28, 8, 5, 3);

        // draw body
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        rect(this.x_pos - 8, this.y_pos - 50, 17, 22, 5);
        rect(this.x_pos - 8, this.y_pos - 28, 17, 8, 5);

        // draw legs
        // left
        rect(this.x_pos - 11.5, this.y_pos - 4, 10, 6, 3);
        rect(this.x_pos - 8.5, this.y_pos - 20, 10, 8, 3);
        rect(this.x_pos - 10, this.y_pos - 12, 10, 8, 3);

        //right
        rect(this.x_pos + 1, this.y_pos - 12, 10, 8, 3);
        rect(this.x_pos - 1, this.y_pos - 20, 10, 8, 3);
        rect(this.x_pos + 2, this.y_pos - 4, 10, 6, 3);

        // draw arms
        rect(this.x_pos - 9, this.y_pos - 48, 8, 10, 3);
        rect(this.x_pos - 9, this.y_pos - 38, 8, 10, 3);
        rect(this.x_pos - 9, this.y_pos - 28, 8, 5, 3);

        // draw logo

        noStroke();
        fill(255, 0, 255);
        ellipse(this.x_pos + 5, this.y_pos - 45, 5, 5);

        fill(244, 243, 245);
        ellipse(this.x_pos + 5, this.y_pos - 45, 2, 2);

        //draw cable
        fill(70, 130, 180);
        stroke(0);
        strokeWeight(0.2);
        beginShape();
        curveVertex(this.x_pos - 12, this.y_pos - 48);
        curveVertex(this.x_pos - 13, this.y_pos - 48);
        curveVertex(this.x_pos - 13, this.y_pos - 55);
        curveVertex(this.x_pos - 10, this.y_pos - 62);
        curveVertex(this.x_pos - 10, this.y_pos - 60);
        curveVertex(this.x_pos - 12, this.y_pos - 55);
        endShape(CLOSE);
      } else if (isFalling || isPlummeting) {
        // draw face

        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        ellipse(this.x_pos, this.y_pos - 63, 20, 20);

        strokeWeight(2);
        stroke(102, 84, 86);
        fill(255, 199, 169);
        ellipse(this.x_pos, this.y_pos - 63, 10, 10);

        // draw eyes
        noStroke();
        fill(102, 84, 86);
        ellipse(this.x_pos - 2, this.y_pos - 63, 2, 3);
        ellipse(this.x_pos + 2, this.y_pos - 63, 2, 3);

        // draw body
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        rect(this.x_pos - 10.5, this.y_pos - 53, 22, 22, 5);
        rect(this.x_pos - 10.5, this.y_pos - 31, 22, 8, 5);

        // draw legs
        rect(this.x_pos - 10.5, this.y_pos - 23, 10, 8, 3);
        rect(this.x_pos + 1, this.y_pos - 23, 10, 8, 3);
        rect(this.x_pos - 11.5, this.y_pos - 15, 10, 8, 3);
        rect(this.x_pos + 2, this.y_pos - 15, 10, 8, 3);
        rect(this.x_pos - 12, this.y_pos - 7, 10, 6, 3);
        rect(this.x_pos + 2.5, this.y_pos - 7, 10, 6, 3);

        // draw arms
        // left
        rect(this.x_pos - 19, this.y_pos - 51, 8, 10, 3);
        rect(this.x_pos - 22, this.y_pos - 41, 8, 10, 3);
        rect(this.x_pos - 24, this.y_pos - 31, 8, 5, 3);

        //right
        rect(this.x_pos + 12, this.y_pos - 51, 8, 10, 3);
        rect(this.x_pos + 15, this.y_pos - 41, 8, 10, 3);
        rect(this.x_pos + 17, this.y_pos - 31, 8, 5, 3);

        // draw logo

        noStroke();
        fill(255, 0, 255);
        ellipse(this.x_pos + 6, this.y_pos - 45, 5, 5);

        fill(244, 243, 245);
        ellipse(this.x_pos + 6, this.y_pos - 45, 2, 2);
      } else {
        // draw face

        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        ellipse(this.x_pos, this.y_pos - 60, 20, 20);

        strokeWeight(2);
        stroke(102, 84, 86);
        fill(255, 199, 169);
        ellipse(this.x_pos, this.y_pos - 60, 10, 10);

        // draw eyes
        noStroke();
        fill(102, 84, 86);
        ellipse(this.x_pos - 2, this.y_pos - 60, 2, 3);
        ellipse(this.x_pos + 2, this.y_pos - 60, 2, 3);

        // draw body
        strokeWeight(0.3);
        stroke(102, 84, 86);
        fill(244, 243, 245);
        rect(this.x_pos - 10.5, this.y_pos - 50, 22, 22, 5);
        rect(this.x_pos - 10.5, this.y_pos - 28, 22, 8, 5);

        // draw legs
        rect(this.x_pos - 10.5, this.y_pos - 20, 10, 8, 3);
        rect(this.x_pos + 1, this.y_pos - 20, 10, 8, 3);
        rect(this.x_pos - 11, this.y_pos - 12, 10, 8, 3);
        rect(this.x_pos + 1.5, this.y_pos - 12, 10, 8, 3);
        rect(this.x_pos - 11.5, this.y_pos - 4, 10, 6, 3);
        rect(this.x_pos + 2, this.y_pos - 4, 10, 6, 3);

        // draw arms
        rect(this.x_pos - 19, this.y_pos - 48, 8, 10, 3);
        rect(this.x_pos - 19, this.y_pos - 38, 8, 10, 3);
        rect(this.x_pos - 19, this.y_pos - 28, 8, 5, 3);
        rect(this.x_pos + 12, this.y_pos - 48, 8, 10, 3);
        rect(this.x_pos + 12, this.y_pos - 38, 8, 10, 3);
        rect(this.x_pos + 12, this.y_pos - 28, 8, 5, 3);

        // draw logo

        noStroke();
        fill(255, 0, 255);
        ellipse(this.x_pos + 6, this.y_pos - 45, 5, 5);

        fill(244, 243, 245);
        ellipse(this.x_pos + 6, this.y_pos - 45, 2, 2);
      }
    },
  };

  //for each alien, we register a callback function to be called when the alien collides with the player
  function handleOverlap() {
    lives -= 1;
    if (lives > 0) {
      startGame();
    }
    if (dieSoundPlaying == false) {
      dieSound.play();
      dieSoundPlaying = true;
    }
  }

  alien1.onOverlap(character.sprite, handleOverlap);

  alien2.onOverlap(character.sprite, handleOverlap);

  alien3.onOverlap(character.sprite, handleOverlap);

  alien4.onOverlap(character.sprite, handleOverlap);
  
  // Variable to control the background scrolling.
  scrollPos = 0;

  // Variable to store the real position of the gameChar in the game
  // world. Needed for collision detection.
  gameChar_world_x = character.x_pos - scrollPos;

  // Boolean variables to control the movement of the game character.
  
  isLeft = false;
  isRight = false;
  isFalling = false;
  isPlummeting = false;

  // ensure sounds don't repeat forever by adding some booleans as terminating condition
  dieSoundPlaying = false;
  overSoundPlaying = false;
  winSoundPlaying = false;
  completeSoundPlaying = false;

  // Initialise objects of scenery items.

  // Background colors
  color1 = color(48, 44, 80);
  color2 = color(58, 66, 94);

  // Rocket Object (includes everything related to the rocket)
  rocket = {
    x_pos: 4600, // position of rocket
    isReached: false,
    // Function to check the flagpole
    checkRocket: function () {
      let d = abs(this.x_pos + 25 - gameChar_world_x);
      if (d < 80) {
        this.isReached = true;
        gameCompleted = true;
      }
    },
    // Method to draw Rocket logo
    starLogo: function (x, y, radius1, radius2, npoints) {
      let angle = TWO_PI / npoints;
      let halfAngle = angle / 2.0;
      beginShape();
      for (let a = 0; a < TWO_PI; a += angle) {
        let sx = x + cos(a) * radius2;
        let sy = y + sin(a) * radius2;
        vertex(sx, sy);
        sx = x + cos(a + halfAngle) * radius1;
        sy = y + sin(a + halfAngle) * radius1;
        vertex(sx, sy);
      }
      endShape(CLOSE);
    },
    //Method to draw rocket
    renderRocket: function () {
      push();//saves the current drawing style settings and transformations

      // body of the rocket
      fill(195, 195, 195);
      rect(this.x_pos, floorPos_y - 20, 50, -190);
      // bottom of the rocket
      fill(160);
      rect(this.x_pos - 10, floorPos_y, 70, -20);

      // right wing
      beginShape();
      vertex(this.x_pos + 50, floorPos_y - 40);
      vertex(this.x_pos + 130, floorPos_y - 40);
      vertex(this.x_pos + 130, floorPos_y - 65);
      vertex(this.x_pos + 50, floorPos_y - 190);
      endShape();

      // left wing
      beginShape();
      vertex(this.x_pos, floorPos_y - 40);
      vertex(this.x_pos - 80, floorPos_y - 40);
      vertex(this.x_pos - 80, floorPos_y - 65);
      vertex(this.x_pos, floorPos_y - 190);
      endShape();

      // draw cockpit
      stroke(195, 195, 195);
      strokeWeight(8);
      strokeJoin(ROUND);
      fill(195, 195, 195);
      triangle(
        this.x_pos + 4,
        floorPos_y - 210,
        this.x_pos + 46,
        floorPos_y - 210,
        this.x_pos + 25,
        floorPos_y - 260,
        0.5
      );

      // draw window
      strokeWeight(10);
      strokeCap(SQUARE);
      stroke(147, 112, 219);
      arc(this.x_pos + 25, floorPos_y - 180, 20, 20, PI, 0);

      // draw logo

      noStroke();
      fill(255);
      this.starLogo(this.x_pos - 30, floorPos_y - 80, 6, 13, 5);
      this.starLogo(this.x_pos + 80, floorPos_y - 80, 6, 13, 5);

      // draw door
      strokeWeight(1);
      fill(195, 195, 195);

      if (!this.isReached) {
        stroke(0);
        rect(this.x_pos + 10, floorPos_y - 85, 30, 60);
      } else {
        fill(0);
        stroke(0);
        rect(this.x_pos + 10, floorPos_y - 85, 30, 60);
      }
      pop();  // Insert pop to reset the drawing to its previous settings.

    },
  };

  // Oxygen bar object
  oxygen = {
    draw: function () { //draw oxygen bar
      if (air > 50) {
        noStroke();
        fill(156, 229, 246);
        rect(10, 10, map(air, 0, maxAir, 0, 200), 20);
        textSize(14);
        text("Oxygen Level", 220, 25);

        stroke(230);
        strokeWeight(4);
        noFill();
        rect(10, 10, 200, 20, 5);
      } else {
        if (frameCount % 30 === 0) {
          oxygenDisplay = !oxygenDisplay; // flash oxygen bar and text when oxygen is below 50
        }
        if (oxygenDisplay && lives > 0) { 
          noStroke();
          fill(209, 29, 31);
          rect(10, 10, map(air, 0, maxAir, 0, 200), 20);
          textSize(14);
          text("Collect oxygen to survive", 220, 25);
          stroke(230);
          strokeWeight(4);
          noFill();
          rect(10, 10, 200, 20, 5);
          strokeWeight(0);
        }
      }
    },
  };

  // Stars object with methods to draw and create stars
  stars = {
    array: [], 
    createStars: function () { //add items to array for them to be drawn later
      for (let i = 0; i < 1000; i++) {
        this.array[i] = {
          x: random(width),
          y: random(height),
          size: random(0.3, 2),
          t: random(TAU),
        };
      }
    },
    drawStars: function () { //draw stars
      for (let i = 0; i < this.array.length; i++) {
        this.array[i].t += 0.1;
        let scale = this.array[i].size + sin(this.array[i].t) * 2;
        fill(255);
        noStroke();
        ellipse(this.array[i].x, this.array[i].y, scale, scale);
      }
    },
  };

  // Planets object
  planets = {
    array: [
      {
        x_pos: 100,
        y_pos: 150,
        size: random(35, 60),
        color: color(random(0, 255), random(0, 255), random(0, 255)),
      },
      {
        x_pos: 1200,
        y_pos: 200,
        size: random(35, 60),
        color: color(random(0, 255), random(0, 255), random(0, 255)),
      },
      {
        x_pos: 2680,
        y_pos: 100,
        size: random(35, 60),
        color: color(random(0, 255), random(0, 255), random(0, 255)),
      },
    ],
    // Function to draw planets
    draw: function () {
      for (let i = 0; i < this.array.length; i++) {
        fill(this.array[i].color);
        ellipse(
          this.array[i].x_pos,
          this.array[i].y_pos,
          this.array[i].size,
          this.array[i].size
        );
        fill(160);
        ellipse(this.array[i].x_pos - 10, this.array[i].y_pos - 10, 5, 5);
        ellipse(this.array[i].x_pos + 10, this.array[i].y_pos + 5, 8, 8);
      }
    },
  };

  // Trees object
  trees = {
    array: [-150, -400, -858, -1282, -2000, 300, 1858, 2282, 3000],
    // Method to draw tree objects.
    draw: function (colorR, colorG, colorB) { // the trees colors change per level
      for (let i = 0; i < this.array.length; i++) {
        fill(colorR, colorG, colorB);
        rect(this.array[i], floorPos_y, 10, -360);
        triangle(
          this.array[i],
          floorPos_y - 40,
          this.array[i],
          floorPos_y - 25,
          this.array[i] - 60,
          floorPos_y - 55
        );
        triangle(
          this.array[i],
          floorPos_y - 160,
          this.array[i],
          floorPos_y - 145,
          this.array[i] - 60,
          floorPos_y - 195
        );
        triangle(
          this.array[i],
          floorPos_y - 250,
          this.array[i],
          floorPos_y - 235,
          this.array[i] - 60,
          floorPos_y - 285
        );
        triangle(
          this.array[i],
          floorPos_y - 120,
          this.array[i],
          floorPos_y - 105,
          this.array[i] + 60,
          floorPos_y - 155
        );
        triangle(
          this.array[i],
          floorPos_y - 320,
          this.array[i],
          floorPos_y - 305,
          this.array[i] + 60,
          floorPos_y - 355
        );
        triangle(
          this.array[i],
          floorPos_y - 210,
          this.array[i],
          floorPos_y - 195,
          this.array[i] + 60,
          floorPos_y - 245
        );
      }
    },
  };

  // Mountains object
  mountains = {
    array: [
      {
        x_pos: 150,
        y_pos: 150,
        width: 450,
        height: floorPos_y - 150,
      },
      {
        x_pos: 980,
        y_pos: 200,
        width: 450,
        height: floorPos_y - 200,
      },
      {
        x_pos: 1600,
        y_pos: 150,
        width: 450,
        height: floorPos_y - 150,
      },
      {
        x_pos: 2980,
        y_pos: 200,
        width: 450,
        height: floorPos_y - 200,
      },
    ],
    // Function to draw mountains objects.
    draw: function (colorR, colorG, colorB) {// the mountain colors change per level
      for (let i = 0; i < this.array.length; i++) {
        fill(53, 53, 67);
        triangle(
          this.array[i].x_pos,
          this.array[i].y_pos + this.array[i].height,
          this.array[i].x_pos + 2 * this.array[i].width,
          this.array[i].y_pos + this.array[i].height,
          this.array[i].x_pos + this.array[i].width,
          this.array[i].y_pos - 0.5 * this.array[i].height
        );
        fill(colorR, colorG, colorB);
        triangle(
          this.array[i].x_pos + this.array[i].width / 2,
          this.array[i].y_pos + this.array[i].height,
          this.array[i].x_pos + 2 * this.array[i].width,
          this.array[i].y_pos + this.array[i].height,
          this.array[i].x_pos + this.array[i].width,
          this.array[i].y_pos - 0.5 * this.array[i].height
        );
        fill(53, 53, 67);
        triangle(
          this.array[i].x_pos,
          this.array[i].y_pos,
          this.array[i].x_pos - this.array[i].width / 2,
          this.array[i].y_pos + this.array[i].height,
          this.array[i].x_pos + this.array[i].width / 2,
          this.array[i].y_pos + this.array[i].height
        );
        fill(colorR, colorG, colorB);
        triangle(
          this.array[i].x_pos,
          this.array[i].y_pos,
          this.array[i].x_pos - this.array[i].width / 3,
          this.array[i].y_pos + this.array[i].height,
          this.array[i].x_pos + this.array[i].width / 2,
          this.array[i].y_pos + this.array[i].height
        );
      }
    },
  };

  // Canyon object
  canyons = {
    array: [
      {
        x_pos: 90,
        width: 70,
      },
      {
        x_pos: 990,
        width: 70,
      },
      {
        x_pos: 1300,
        width: 60,
      },
      {
        x_pos: 1680,
        width: 70,
      },
      {
        x_pos: 2680,
        width: 70,
      },
      {
        x_pos: 3200,
        width: 70,
      },
    ],
    // Function to draw canyon objects.
    draw: function (t_canyon) {
      fill(64, 64, 64);
      rect(t_canyon.x_pos, floorPos_y, t_canyon.width, height);
    },
    // Function to check character is over a canyon.
    check: function (t_canyon) {
      if (
        t_canyon.x_pos + 15 < gameChar_world_x &&
        gameChar_world_x < t_canyon.x_pos + t_canyon.width - 15 &&
        character.y_pos >= floorPos_y
      ) {
        isPlummeting = true;
        if (dieSoundPlaying == false) {
          dieSound.play();
          dieSoundPlaying = true;
        }
      }
    },
  };

  // platforms object

  platforms = {
    array: [],
    create: function (x, y, length) {
      let p = {
        x: x,
        y: y,
        length: length,
        draw: function () {
          fill(219);
          rect(this.x, this.y, this.length, 20, 5);
        },
        checkContact: function (gc_x, gc_y) {
          if (gc_x > this.x && gc_x < this.x + this.length) {
            let d = this.y - gc_y;
            if (d >= 0 && d < 5) {
              return true;
            }
          }
          return false;
        },
      };
      return p;
    },
  };

  //Create and draw the platforms in different locations across the game space

  platforms.array.push(platforms.create(300, floorPos_y - 80, 200));
  platforms.array.push(platforms.create(1500, floorPos_y - 80, 200));
  platforms.array.push(platforms.create(1800, floorPos_y - 150, 200));
  platforms.array.push(platforms.create(3400, floorPos_y - 80, 200));
  platforms.array.push(platforms.create(3600, floorPos_y - 130, 200));
  platforms.array.push(platforms.create(3800, floorPos_y - 180, 200));

  //Stones constructor function
  function Stones(canyons) {
    this.array = [];
    this.createArray = function () {
      let tempArray = [];
      for (let i = 0; i < 25; i++) {
        tempArray.push({
          x_pos: random(0, 2200),
          width: random(10, 20),
          height: random(3, 7),
        });
      }

      // Check every stone to ensure the stone isn't drawn within the canyon
      for (let i = 0; i < tempArray.length; i++) {
        let needsRemoval = false;
        // Check every canyon
        for (let j = 0; j < canyons.length; j++) {
          // If the right side of the stone is inside the canyon OR the left side of the stone is inside the canyon
          let leftSideStone = tempArray[i].x_pos;
          let rightSideStone = tempArray[i].x_pos + tempArray[i].width;
          let leftSideCanyon = canyons[j].x_pos;
          let rightSideCanyon = canyons[j].x_pos + canyons[j].width;

          if (
            (leftSideStone < rightSideCanyon &&
              leftSideStone > leftSideCanyon) ||
            (rightSideStone > leftSideCanyon &&
              rightSideStone < rightSideCanyon)
          ) {
            needsRemoval = true;
          }
        }
        // If the stone wasn't marked for removal, we add it to the final list
        if (!needsRemoval) {
          this.array.push(tempArray[i]);
        }
      }
    };
    // Function to draw stones objects.
    this.draw = function () {
      noStroke();
      fill(178, 179, 165);
      for (let i = 0; i < this.array.length; i++) {
        beginShape();
        curveVertex(this.array[i].x_pos, floorPos_y);
        curveVertex(this.array[i].x_pos, floorPos_y);
        curveVertex(this.array[i].x_pos + 3, floorPos_y - this.array[i].height);
        curveVertex(
          this.array[i].x_pos + this.array[i].width - 3,
          floorPos_y - this.array[i].height
        );
        curveVertex(this.array[i].x_pos + this.array[i].width, floorPos_y);
        curveVertex(this.array[i].x_pos + this.array[i].width, floorPos_y);
        endShape();
      }
    };

    this.createArray();
  }

  // Call my stones constructor
  stones = new Stones(canyons.array);

  // Collectable Oject
  collectable = {
    array: [
      {
        x_pos: -200,
        y_pos: 350,
        size: 20,
        isFound: false,
      },
      {
        x_pos: canyons.array[0].x_pos + canyons.array[0].width / 2,
        y_pos: 350,
        size: 20,
        isFound: false,
      },
      {
        x_pos: canyons.array[1].x_pos + canyons.array[1].width / 2,
        y_pos: 300,
        size: 20,
        isFound: false,
      },
      {
        x_pos: canyons.array[2].x_pos + canyons.array[2].width / 2,
        y_pos: 280,
        size: 20,
        isFound: false,
      },
      {
        x_pos: platforms.array[0].x + platforms.array[0].length / 2,
        y_pos: 280,
        size: 20,
        isFound: false,
      },
      {
        x_pos: platforms.array[1].x + platforms.array[1].length / 2,
        y_pos: 280,
        size: 20,
        isFound: false,
      },
      {
        x_pos: platforms.array[2].x + platforms.array[2].length / 2,
        y_pos: 230,
        size: 20,
        isFound: false,
      },
      {
        x_pos: platforms.array[3].x + platforms.array[3].length / 2,
        y_pos: 230,
        size: 20,
        isFound: false,
      },
      {
        x_pos: platforms.array[4].x + platforms.array[4].length / 2,
        y_pos: 230,
        size: 20,
        isFound: false,
      },
      {
        x_pos: platforms.array[5].x + platforms.array[5].length / 2,
        y_pos: 200,
        size: 20,
        isFound: false,
      },
    ],
    // Method to check character has collected an item.
    check: function (t_collectable) {
      let dist1 = dist(
        t_collectable.x_pos,
        t_collectable.y_pos,
        gameChar_world_x,
        character.y_pos
      );
      let dist2 = dist(
        t_collectable.x_pos + t_collectable.size,
        t_collectable.y_pos + 40,
        gameChar_world_x,
        character.y_pos
      );
      if (dist1 < 60 || dist2 < 60) {
        t_collectable.isFound = true;
        air += t_collectable.score;
        collectableSound.play();
      }
    },
    //Method to draw oxygen collectable
    create: function (t_collectable, color) {
      this.color = color;
      fill(0);
      rect(t_collectable.x_pos - 5, t_collectable.y_pos - 14, 10, 5, 9);
      noStroke();
      fill(color);
      ellipse(
        t_collectable.x_pos,
        t_collectable.y_pos,
        t_collectable.size,
        t_collectable.size
      );
      rect(
        t_collectable.x_pos - t_collectable.size / 2,
        t_collectable.y_pos,
        t_collectable.size,
        40
      );
      textSize(10);
      fill(0);
      strokeWeight(0.1);
      text("O2", t_collectable.x_pos - 7, t_collectable.y_pos + 20);
      noFill();
      stroke(0);
      strokeWeight(0.9);
      beginShape();
      curveVertex(
        t_collectable.x_pos - t_collectable.size / 2,
        t_collectable.y_pos
      );
      curveVertex(
        t_collectable.x_pos - t_collectable.size / 2,
        t_collectable.y_pos
      );
      curveVertex(t_collectable.x_pos - 4, t_collectable.y_pos + 2);
      curveVertex(t_collectable.x_pos + 4, t_collectable.y_pos + 2);
      curveVertex(
        t_collectable.x_pos + t_collectable.size / 2,
        t_collectable.y_pos
      );
      curveVertex(
        t_collectable.x_pos + t_collectable.size / 2,
        t_collectable.y_pos
      );
      endShape();
      beginShape();
      curveVertex(
        t_collectable.x_pos - t_collectable.size / 2,
        t_collectable.y_pos + 30
      );
      curveVertex(
        t_collectable.x_pos - t_collectable.size / 2,
        t_collectable.y_pos + 30
      );
      curveVertex(t_collectable.x_pos - 4, t_collectable.y_pos + 32);
      curveVertex(t_collectable.x_pos + 4, t_collectable.y_pos + 32);
      curveVertex(
        t_collectable.x_pos + t_collectable.size / 2,
        t_collectable.y_pos + 30
      );
      curveVertex(
        t_collectable.x_pos + t_collectable.size / 2,
        t_collectable.y_pos + 30
      );
      endShape();
    },
    draw: function(){
      for (let i = 0; i < this.array.length; i++) {
        if (this.array[i].isFound == false) {
          if (i % 5 == 0) {
            this.create(collectable.array[i], color(175, 238, 238));
            this.check(collectable.array[i]);
            this.array[i].score = 25;
          } else if (i % 5 > 0) {
            if (level == 2) {
              this.create(collectable.array[i], color(247, 218, 98));
              this.check(collectable.array[i]);
              this.array[i].score = 15;
            } else if (level == 1) {
              this.create(collectable.array[i], color(122, 101, 168));
              this.check(collectable.array[i]);
              this.array[i].score = 15;
            }
          }
        }
      }
    }
  };

  livesCounter = {
    draw: function(){
      for (let i = 0; i < lives; i++) {
          fill(255);
          ellipse(795 + i * 80, 20, 20, 20);
          ellipse(813 + i * 80, 20, 20, 20);
          triangle(784 + i * 80, 21, 806 + i * 80, 46, 825 + i * 80, 21);
          fill(255, 0, 0);
          ellipse(800 + i * 80, 20, 20, 20);
          ellipse(818 + i * 80, 20, 20, 20);
          triangle(789 + i * 80, 21, 810 + i * 80, 46, 829 + i * 80, 21);
        }
    }
  }


  
  // call stars set up function
  stars.createStars();

  gameCompleted = false;
}
